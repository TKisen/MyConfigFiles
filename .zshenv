typeset -U path PATH
path=(~/.local/bin ~/.cabal/bin ~/opt/activemq/bin ~/opt/Postman /.cargo/bin /.yarn/bin /.config/yarn/global/node_modules/.bin  /usr/lib/node_modules/node/lib/node_modules/node/lib/node_modules/node/bin /opt/Hyper /opt/maven/bin /opt/jdk/bin $path)
export PATH

export JAVA_HOME="/opt/jdk"
export M2_HOME="/opt/maven"
export M2="/opt/maven/bin"
export NVM_DIR="/home/takuya/.nvm"

export BROWSER="firefox"
export EDITOR="nvim"
export GOPATH="$HOME/go"

LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';
export LS_COLORS
